@extends('layouts.main')

@section('body')
<div class="row mt-4">
    <div class="card-body text-center">
        <div class="col-lg-auto col-sm-12">
            <div class="row">
            <div class="col-sm-12">
                <h2 class="font-weight-bold">
                    TENTANG KAMI
                </h2>
            </div>
            <div class="col-sm-5"></div>                    
            <div class="col-sm-2">
                <div style="border: 2px red solid;"></div>
            </div>
            <div class="col-sm-5"></div>                    
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/src/banner-fs.jpg')}}" style="max-height: 600px;" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">
                        <h3 class="text-center">
                           <b> FAKHRI SERVICE </b> 
                        </h3>
                        <h6 class="text-muted">
                            Merupakan transformasi dari Speed Tyre Shop dan fokus melayani penjualan barang dan jasa seputar Ban, Oli, Service untuk segala jenis motor. Dengan pengalaman yang telah didapat dari bengkel terdahulu, kami siap memberikan pelayanan yang lebih lengkap dan menyeluruh dengan berbagai keunggulan baru bagi setiap pelanggan.
                        </h6>
                    </p>
                    <p class="card-text">
                        <h5>
                            <b>VISI</b>
                        </h5>
                        <h6 class="text-muted">
                            Menjadi bengkel terkemuka dengan memiliki layanan servis yang dapat diandalkan dan menjual spareparts berkualitas.
                        </h6>
                    </p>
                    <p class="card-text">
                        <h5>
                            <b>MISI</b>
                        </h5>
                        <h6 class="text-muted">
                            Memenuhi kebutuhan pelanggan dengan memberikan pelayanan jasa perbaikan kendaraan yang cepat dan memuaskan serta menciptakan lingkungan kerja yang nyaman bagi pelanggan dan karyawan.
                        </h6>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card-body text-center">
            <div class="col-lg-auto col-sm-12">
                <div class="row">
                <div class="col-sm-12">
                    <h2 class="font-weight-bold">
                        PELAYANAN YANG KAMI BERIKAN
                    </h2>
                </div>
                <div class="col-sm-5"></div>                    
                <div class="col-sm-2">
                    <div style="border: 2px red solid;"></div>
                </div>
                <div class="col-sm-5"></div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card" style="background-color: #b22222;">
                <div class="card-body text-center text-white">
                    <!-- <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/tentang/icon-easy.png')}}" style="height: 100%; width: 50px" alt="Card image cap"> -->
                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-get-pocket fa-3x"></i>
                    <h6 class="text-center bold">LAYANAN DENGAN KENYAMANAN</h6>
                    <div class="mt2">
                        <p>
                            Tidak ada lagi membuang waktu di bengkel motor.
                            Mekanik kami dapat datang kepada Anda di mana saja, kapan saja, bahkan pada hari Sabtu dan Minggu.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center mb-1">
                    <!-- <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/tentang/icon-pricing.png')}}" style="height: 100%; width: 50px" alt="Card image cap"> -->
                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-money fa-3x"></i>
                    <h6 class="text-center bold">HARGA KOMPETITIF & TRANSPARAN</h6>
                    <p>
                        Tidak hanya harga kami yang transparan, itu juga hingga 30% lebih murah dibandingkan dengan bengkel lain.
                        Pesan dengan percaya diri!
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="background-color: #b22222;">
                <div class="card-body text-center text-white mb-4">
                    <!-- <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/tentang/icon-guaranteed.png')}}" style="height: 100%; width: 50px" alt="Card image cap"> -->
                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-check-square-o fa-3x"></i>
                    <h6 class="text-center bold">GARANSI LAYANAN TERBAIK</h6>
                    <p>
                        Semua mekanik kami berpengalaman dan terlatih. Layanan kami disertai dengan garansi 3 bulan / 3.000 km.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection