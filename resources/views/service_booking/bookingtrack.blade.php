@extends('layouts.main')

@section('body')
<div class="row mt-4">
    <div class="card-body text-center">
        <div class="col-lg-auto col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="font-weight-bold">
                        TRACK BOOKING
                    </h2>
                </div>
                <div class="col-sm-5"></div>                    
                    <div class="col-sm-2">
                        <div style="border: 2px red solid;"></div>
                    </div>
                <div class="col-sm-5"></div>                    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="row">
            <div class="col"></div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="mt-0 text-center">
                            Masukkan nomor ponsel atau alamat email Anda dan ID pemesanan di bawah ini untuk melihat status pemesanan layanan Anda saat ini. atau log in ke akun anda 
                            untuk melihat detail pemesanan anda.
                            <br>
                            <span> <a href="{{url('/')}}"> Sudah punya akun? Login disini </a></span> 
                        </h5>
                        <div class="form-group">
                            <label>Nomor Hanphone atau Email</label>
                            <input type="text" class="form-control" required placeholder="Nomor Hanphone atau Email"/>
                            <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                        </div>
                        <div class="form-group">
                            <label>Booking ID</label>
                            <input type="text" class="form-control" required placeholder="Booking Id"/>
                            <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div> <!-- end col -->
        </div> <!-- end row -->
        <div class="row">
            <div class="col"></div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="">
                            <a href="#" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                                <h6 class="text-center mt-3">Submit</h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div> <!-- end col -->
        </div>
    </div> <!-- end container -->
</div>
<script>

</script>
@endsection