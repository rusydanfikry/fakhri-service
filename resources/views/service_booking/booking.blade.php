@extends('layouts.main')

@section('body')
<div class="row mt-4">
    <div class="card-body text-center">
        <div class="col-lg-auto col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="font-weight-bold">
                        SERVICE BOOKING
                    </h2>
                </div>
                <div class="col-sm-5"></div>                    
                <div class="col-sm-2">
                    <div style="border: 2px red solid;"></div>
                </div>
                <div class="col-sm-5"></div>                    
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                        Selamat datang di Service Booking Fakhri Service, Cukup Login, dan selesaikan booking dengan 2 steps.
                    </p>
                    <ul>
                        <li>
                            Step 1: Login ke Akun Anda
                        </li>
                        <li>
                            Step 2: Pilih tanggal dan waktu
                        </li>
                    </ul>
                    <p>
                        Anda akan diberi tahu sesegera mungkin setelah status pemesanan Anda dikonfirmasi.
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h3 class="mt-0 header-title">
                        <a href="{{url('/')}}">
                            Sudah punya akun? Log in sekarang.
                        </a>
                    </h3>
                    <h4>
                        <b>
                            Verifikasi Nomor Handphone
                        </b>
                    </h4>
                    <div class="form-group">
                        <label>Nomor Hanphone</label>
                        <input type="number" class="form-control" required placeholder="Nomor Handphone"/>
                        <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                    </div>
                    <div class="form-group">
                        <label>Masukkan OTP</label>
                        <input type="text" class="form-control" required placeholder="Masukkan OTP"/>
                        <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                    </div>
                    <h4>
                        <b>
                            Lengkapi informasi anda
                        </b>
                    </h4>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" required placeholder="Username"/>
                        <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="Email"/>
                        <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                    </div>
                    <div class="form-group">
                        <label>Nomor Kendaraan Bermotor</label>
                        <input type="text" class="form-control" required placeholder="Nomor Kendaraan Bermotor"/>
                        <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                    </div>
                    <div class="form-group">
                        <label>Pilih Tanggal</label>
                        <div>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div><!-- input-group -->
                            <label><span style="color: red;"><small>*Wajib Diisi</small></span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <a href="#" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                            <h6 class="text-center mt-3">Request OTP</h6>
                        </a>
                    </div>
                    <div class="mt-3">
                        <a href="">Klik disini untuk resend OTP</a>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <a href="#" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                            <h6 class="text-center mt-3">Submit</h6>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
</div> 
<script>

</script>
@endsection