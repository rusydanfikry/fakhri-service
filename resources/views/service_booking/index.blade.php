@extends('layouts.main')

@section('body')
<div class="row mt-4">
    <div class="card-body text-center">
        <div class="col-lg-auto col-sm-12">
            <div class="row">
            <div class="col-sm-12">
                <h2 class="font-weight-bold">
                    SERVICE BOOKING
                </h2>
            </div>
            <div class="col-sm-5"></div>                    
            <div class="col-sm-2">
                <div style="border: 2px red solid;"></div>
            </div>
            <div class="col-sm-5"></div>                    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/service_booking/service-pic1.jpg')}}" alt="Card image cap">
                <div class="card-body">
                    <p>
                        Service Booking Fakhri Service
                    </p>
                    <p> 
                        Cukup Login, dan selesaikan booking dengan 2 steps untuk membuat pemesanan.
                    </p>
                    <p>
                        Belum punya akun? 
                        <b>
                            <u>
                                <a href="{{url('/register')}}">Daftar Sekarang!</a>
                            </u>
                        </b>
                    </p>
                    <br>
                    <br>
                    <div class="mt-3">
                        <a href="{{url('/servicebooking/booking')}}" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                            <h6 class="text-center mt-3">Service Booking</h6>
                        </a>
                    </div>
                </div>
                <div style="border: 2px red solid;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid" src="{{asset('user_template/assets/images/service_booking/service-pic2.jpg')}}" alt="Card image cap">
                <div class="card-body">
                    <p>
                        Masuk untuk melihat/mengedit detail pemesanan Anda
                    </p>
                    <p>
                        Belum punya akun?                         <b>
                            <u>
                                <a href="{{url('/register')}}">Daftar Sekarang!</a>
                            </u>
                        </b> 
                    </p>
                    <p>
                        Anda juga dapat melihat/mengedit detail pemesanan Anda dengan memasukkan nomor handphonde dan booking ID.
                    </p>
                    <br>
                    <div class="mt-3">
                        <a href="{{url('/servicebooking/bookingtrack')}}" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                            <h6 class="text-center mt-3">Track Booking</h6>
                        </a>
                    </div>
                </div>
                <div style="border: 2px red solid;"></div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection