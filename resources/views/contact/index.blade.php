@extends('layouts.main')

@section('body')
<body>
    <div class="row mt-4">
        <div class="card-body text-center">
            <div class="col-lg-auto col-sm-12">
                <div class="row">
                <div class="col-sm-12">
                    <h2 class="font-weight-bold">
                        Kontak Kami
                    </h2>
                </div>
                <div class="col-sm-5"></div>                    
                <div class="col-sm-2">
                    <div style="border: 2px red solid;"></div>
                </div>
                <div class="col-sm-5"></div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Nama Lengkap</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="text" placeholder="Nama Lengkap">
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Email</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="text" placeholder="Email">
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Username</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="text" placeholder="Username">
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Password</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="text" placeholder="Password">
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Nomor Handphone</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="number" placeholder="Nomor Handphone">
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-12 col-form-label">Pesan</label>
                                            <div class="col-sm-12">
                                                <textarea required class="form-control" rows="5"></textarea>
                                            </div>
                                            <label class="col-sm-12 col-form-label"><small class="text-primary">*Wajib diisi</small></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="#" style="height: 65px;" class="btn btn-primary btn-block waves-effect waves-light">
                                            <h6 class="text-center mt-3">Submit</h6>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end wrapper -->

</body>
@endsection