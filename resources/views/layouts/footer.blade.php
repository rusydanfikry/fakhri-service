<footer class="footer">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-12">
                <label class="col-form-label mr-2">Find Us</label>
                <i class="fa fa-instagram fa-2x btn-dark" style=" border: 2px solid #0f1f3e; padding:5px;"></i>
                <i class="fa fa-facebook-official fa-2x btn-dark" style=" border: 2px solid #0f1f3e; padding:5px;"></i>
                <i class="fa fa-youtube-play fa-2x btn-dark" style=" border: 2px solid #0f1f3e; padding:5px;"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                © 2021 Fakhri Service <span class="d-none d-md-inline-block"> - Crafted with <i class="mdi mdi-heart text-danger"></i> by Delion.</span>
            </div>
        </div>
    </div>
</footer>