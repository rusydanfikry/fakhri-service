@extends('layouts.main')

@section('body')
<div style="background: -webkit-gradient(linear, left top, right top, from(#FFFFF), to(#FFFFF)); background: linear-gradient(to right, #FFFFF, #FFFFF);">
    <div class="container-fluid">
        <div class="row mx-1">
            <div class="col-md-12">
                <div class="page-title-box">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img class="d-block w-100" src="{{asset('user_template/assets/images/src/banner-fs.jpg')}}" alt="First slide">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('user_template/assets/images/src/banner-fs.jpg')}}" alt="Second slide">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('user_template/assets/images/src/banner-fs.jpg')}}" alt="Third slide">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="px-2" style="margin-top: -6em; padding-top: 100px">
    <div class="row mx-1">
        <div class="col-md-10 mx-auto">
            <div class="card py-2">
                <div class="card-body text-center">
                    <div class="row">
                        <div class="col-lg-auto col-sm-12 my-4 mx-auto">
                            <div class="dropdown" style="cursor:pointer; text-align:center;">
                                <a class="text-secondary" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="mx-2"></i> <br>
                                    <div class="font-weight-bold px-4"></div>
                                    <b class="h2">Selamat Datang Di Fakhri Service</b>
                                    <br>
                                    Siap memberikan servis terbaik untuk kendaraan anda, demi kenyamanan dan keamanan berkendara.
                                </a>
                                <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuLink" >

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div id="topnav">
                        <div class="navbar-custom">
                            <div id="navigation">
                                <ul class="navigation-menu">

                                </ul>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row mx-1">
            <div class="col-xl-12 col-md-12">
                <div class="row mb-3">
                    <div class="col-12">
                        <h3 class="m-t-30 m-b-20">Mengapa Harus Fakhri Service?</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-wrench fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Jaminan Produk Original</h5>
                                        <p class="text-white-70 mb-0">Menyediakan spareparts asli & lengkap sesuai kebutuhan kendaraan anda.
                                            <br>

                                        </p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-check-square-o fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Jaminan Servis</h5>
                                        <p class="text-white-70 mb-0">Kepastian pekerjaan,
                                            tenaga kerja professional.</p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    {{-- <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-briefcase fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Modern dan Lengkap</h5>
                                        <p class="text-white-70 mb-0">Servis menggunakan peralatan yang lengkap dan modern.</p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div> --}}
                    <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-calendar-check-o fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Booking Sevice</h5>
                                        <p class="text-white-70 mb-0">Kemudahan booking service, dimana saja dan kapan saja.</p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    {{-- <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-building-o fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Nyaman</h5>
                                        <p class="text-white-70 mb-0">Memiliki fasilitas yang nyaman, serta petugas yang ramah</p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div> --}}
                    <div class="col-xl-4 col-md-6">
                        <div class="card mini-stats">
                            <div class="p-3 mini-stats-content">
                                <div class="media text-light">
                                    <i class="mx-3 my-auto thumb-md rounded-circle fa fa-money fa-3x"></i>
                                    <div class="media-body my-2">
                                        <h5 class="mt-0">Harga Terjangkau</h5>
                                        <p class="text-white-70 mb-0">Memberikan pelayanan service yang murah tapi tidak murahan</p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mx-1">
            <div class="col-xl-12 col-md-12">
                <div class="row mb-3">
                    <div class="col-12">
                        <h3 class="m-t-30 m-b-20">Daftar Sekarang</h3>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="card m-b-20 mini-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div id="carouselExample" class="carousel slide col-md-6" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                          <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
                                          <li data-target="#carouselExample" data-slide-to="1"></li>
                                          <li data-target="#carouselExample" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                          <div class="carousel-item active">
                                            <img class="d-block w-100" src="{{asset('user_template/assets/images/img/IMG_6509.jpg')}}" style="max-height: 400px;" alt="First slide">
                                          </div>
                                          <div class="carousel-item">
                                            <img class="d-block w-100" src="{{asset('user_template/assets/images/img/01d5908f-0b92-45b7-8ca2-bbe401397640.jpg')}}" style="max-height: 400px;" alt="Second slide">
                                          </div>
                                          <div class="carousel-item">
                                            <img class="d-block w-100" src="{{asset('user_template/assets/images/img/IMG_6602.jpg')}}" style="max-height: 400px;" alt="Third slide">
                                          </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                          <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                          <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <div class="col-md-6" style="background-color: #b22222;">                                        
                                        <div class="row">    
                                            <div class="col-md-12 mb-4">    
                                                <h5 class="ml-3 m-t-30 m-b-20 text-white">Daftar sekarang
                                                </h5>
                                                <h6 class="ml-3 text-white">Nikmati promosi, benefits, serta layanan eksklusif Fakhri Service.</h6>
                                            </div>
                                            <div class="col-md-12 mb-4">
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10">
                                                <a href="{{url('/register')}}" style="height: 65px;" class="btn btn-light btn-block waves-effect waves-light">
                                                    <h6 class="text-center mt-3">Daftar Sekarang</h6>
                                                </a>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body mini-stats-content">
                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
\
</script>
@endsection