@extends('layouts.main')

@section('body')
<div class="row mt-4">
    <div class="card-body text-center">
        <div class="col-lg-auto col-sm-12">
            <div class="row">
            <div class="col-sm-12">
                <h2 class="font-weight-bold">
                    Galeri
                </h2>
            </div>
            <div class="col-sm-5"></div>                    
            <div class="col-sm-2">
                <div style="border: 2px red solid;"></div>
            </div>
            <div class="col-sm-5"></div>                    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/01d5908f-0b92-45b7-8ca2-bbe401397640.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6600.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6602.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6599.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6598.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6595.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6593.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6592.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
            <div class="card">
                <img class="card-img-top img-fluid img-thumbnail " src="{{asset('user_template/assets/images/img/IMG_6509.jpg')}}" style="max-height: 260px;" alt="Card image cap">
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection