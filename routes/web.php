<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ServiceBookingController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\AboutController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [RegisterController::class, 'index']);
Route::get('/contact', [ContactController::class, 'index']);
Route::get('/servicebooking', [ServiceBookingController::class, 'index']);
Route::get('/servicebooking/booking', [ServiceBookingController::class, 'booking']);
Route::get('/servicebooking/bookingtrack', [ServiceBookingController::class, 'bookingtrack']);
Route::get('/gallery', [GalleryController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);