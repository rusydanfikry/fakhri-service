<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceBookingController extends Controller
{
   public function index()
   {
    return view('service_booking.index');
   }

   public function booking()
   {
    return view('service_booking.booking');
   }

   public function bookingtrack()
   {
    return view('service_booking.bookingtrack');
   }
}
